package Veterinaria;

import java.io.IOException;

public class Control {

    public void iniciar() {
        Veterinaria veterinaria = new  Veterinaria();

        boolean continuar;
        try {

            veterinaria= veterinaria.deSerializar("datos.txt");
            continuar = EntradaSalida.leerBoolean("Desea ingresar al sistema\n LA VETERINARIA?");
        } catch (Exception e) {
            String usuario = EntradaSalida.leerString("REGISTRACION DEL ADMINISTRADOR.\n\n"
                    + "Ingrese nombre de usuario:");
            if (usuario.equals("")) {
                throw new NullPointerException("ERROR: El usuario no puede ser nulo.");
            }
            String password = EntradaSalida.leerString("Ingrese una contraseña: ");
            if (password.equals("")) {
                throw new NullPointerException("ERROR: La contraseña no puede ser nula.");
            }
            veterinaria.agregarUsuario(new Administrador(usuario, password));
            try {
                veterinaria.serializar("datos.txt");
                EntradaSalida.mostrarString("Usuario y contraseña correctos!");
                EntradaSalida.mostrarString("Vuelva a iniciar el sistema!");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            continuar = false;
        }
        while (continuar) {
            String usuario = EntradaSalida.leerString("Ingrese nombre de usuario: ");
            String password = EntradaSalida.leerString("Ingrese contraseña: ");

            Usuario u = veterinaria.buscarUsuario(usuario + ": " + password);

            if (u == null) {
                EntradaSalida.mostrarString("ERROR! En nombre de usuario o contraseña.");
            } else {
                continuar = u.seguir(veterinaria);  
            }
        }
    }
}
    

