package Veterinaria;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Recepcionista extends Usuario implements Serializable {

    private int cantidadDeVentasRealiz = 0;

    public enum TipoAnimal {
        GATO, PERRO, AVE, EXOTICO
    }

    public enum Mes {
        ENERO, FEBRERO, MARZO, ABRIL, MAYO, JUNIO, JULIO, AGOSTO, SEPTIEMBRE, OCTUBRE, NOVIEMBRE, DICIEMBRE
    }

    public Recepcionista(String username, String password) {
        super(username, password);
    }

    @Override
    public void setPassword(String password) {
        super.setPassword(password);
    }

    @Override
    public String getPassword() {
        return super.getPassword();
    }

    @Override
    public void setUsername(String username) {
        super.setUsername(username);
    }

    @Override
    public String getUsername() {
        return super.getUsername();
    }

    public int getCantidadDeVentasRealiz() {
        return cantidadDeVentasRealiz;
    }

    @Override
    public String toString() {
        return "                Recepcionista                            ";
    }

    public String aString() {
        return super.toString() + "\n";
    }

    @Override
    public void mostrar() {
        EntradaSalida.mostrarString(super.toString() + this.toString());
    }

    @Override
    public boolean seguir(Veterinaria veterinaria) {
        char op;
        boolean tipoUser = false;
        boolean continuar = true;
        do {
            op = EntradaSalida.leerChar(
                    "*********************************************\nFUNCIONES DEL RECEPCIONISTA\n**********************************************\n"
                    + "[1]   Otorgar Turno\n"
                    + "[2]   Vender Producto REGULAR\n"
                    + "[3]   Listado de TURNOS\n"
                    + "[4]   Listado de PRODUCTOS REGULARES\n"
                    + "[5]   Cambiar de Usuario\n"
                    + "[6]   Salir del sistema");

            String r = "RECEPCIONISTA";
            String v = "VETERINARIO";

            switch (op) {
                case '1':
                    otorgarTurno(veterinaria);
                    break;

                case '2':
                    venderProductoRegular(veterinaria);
                    break;

                case '3':
                    veterinaria.mostrarTurnos();
                    break;

                case '4':
                    veterinaria.mostrarProductos(tipoUser);
                    break;

                case '5':
                    continuar = true;
                    break;

                case '6':
                    continuar = false;
                    break;

                default:
                    EntradaSalida.mostrarString("ERROR: Opcion invalida");
                    op = '*';
            } //Fin switch
            if (op >= '1' && op <= '3') {
                try {
                    veterinaria.serializar("datos.txt");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } while (op != '5' && op != '6'); //Fin while
        return continuar;

    }

    public void otorgarTurno(Veterinaria v) {
        ArrayList<Turno> turnos = v.getTurnos();
        ArrayList<Usuario> usuarios = v.getUsuarios();
        ArrayList<Agenda> agendas = v.getAgendas();
        Calendar gc = new GregorianCalendar();
        Agenda a = null;
        Turno t = null;
        Veterinario veterinario = null;
        Usuario u = null;
        String user = null;
        int codAgenda = 0;
        int codTurno = 0;
        boolean confirmar = false;
        int indexT = -1; //-1
        int indexU = -1; //-1
        int indexA = -1; //-1
        indexU = validarVeterinario(v);
        if (indexU != -1) {
            veterinario = (Veterinario) usuarios.get(indexU);
            indexT = validarTurno(v);
            if (indexT != -1) {
                EntradaSalida.mostrarString("La fecha Ingresada no corresponde a rangos establecido para realizar un turno");
                System.out.print("Codigo de turno registrado");
            } else {
                // t = turnos.get(indexT);
                confirmar = EntradaSalida.leerBoolean("Desea confirmar el TURNO?");
            }
            if (confirmar) {
                t = new Turno(codTurno, ingresarFecha(), ingresarTipoAnimal(), true);
                v.addTurno(t);
                indexA = validarAgenda(v);
                if (indexA == -1) {
                    a = new Agenda(indexA, veterinario.getNroVeterinario(), t.getCodTurno());
                    v.agregarAgenda(a);
                }
                //EntradaSalida.mostrarString("El turno no esta disponible por el momento");                    
            }

        } else {
            EntradaSalida.mostrarString("ERROR!: El ID ingresado No se encuentra registrado en el sistema.");
        }
    }

    public int validarVeterinario(Veterinaria v) {
        int indexV = 0;
        int nroVeterinario = 0;
        try {
            nroVeterinario = EntradaSalida.leerInt("Ingrese el ID del Veterinario: ");
        } catch (NumberFormatException ex) {
            EntradaSalida.mostrarString("Ingrese un identificador valido disponible");
        } catch (NullPointerException e) {
            EntradaSalida.mostrarString("Ingrese un identificador valido disponible");
        }
        if (nroVeterinario == 0) {
            EntradaSalida.mostrarString("ERROR: codigo no existe.");
        } else {
            indexV = v.buscarIndexVeterinario(nroVeterinario); //ACA ESTA EL ERROR
            if (indexV == -1) {
                EntradaSalida.mostrarString("Veterinario no encontrado: " + indexV);
            }
        }
        return indexV;
    }

    public int validarTurno(Veterinaria v) {
        int indexT = 0;
        int nroTurno = 0;
        try {
            nroTurno = EntradaSalida.leerInt("Ingrese el ID de Turno: ");
        } catch (NumberFormatException ex) {
            EntradaSalida.mostrarString("Ingrese un identificador valido disponible");
        } catch (NullPointerException e) {
            EntradaSalida.mostrarString("Ingrese un identificador valido disponible");
        }
        if (nroTurno == 0) {
            EntradaSalida.mostrarString("ERROR: codigo no existe.");
        } else {
            indexT = v.buscarIndexTurno(nroTurno); //ACA ESTA EL ERROR
            if (indexT == -1) {
                EntradaSalida.mostrarString("Turno no encontrado: " + indexT);
            }
        }
        return indexT;
    }

    public int validarAgenda(Veterinaria v) {
        int indexA = 0;
        int nroAgenda = 0;
        try {
            nroAgenda = EntradaSalida.leerInt("Ingrese el ID de Agenda: ");
        } catch (NumberFormatException ex) {
            EntradaSalida.mostrarString("Ingrese un identificador valido disponible");
        } catch (NullPointerException e) {
            EntradaSalida.mostrarString("Ingrese un identificador valido disponible");
        }
        if (nroAgenda == 0) {
            EntradaSalida.mostrarString("ERROR: codigo no existe.");
        } else {
            indexA = v.buscarIndexVeterinario(nroAgenda); //ACA ESTA EL ERROR
            if (indexA == 0) {
                EntradaSalida.mostrarString("Veterinario no encontrado");
            }
        }
        return indexA;
    }

    public void venderProductoRegular(Veterinaria v) {
        ArrayList<Producto> productos = v.getProductos();
        ArrayList<Usuario> usuarios = v.getUsuarios();
        ArrayList<Venta> ventas = v.getVentas();
        Venta venta = null;
        Producto p = null;
        Recepcionista r = null;
        Usuario u = null;
        String user = null;
        int codVenta = 0;
        int codProducto = 0;
        boolean confirmar = false;
        int indexP = -1; //-1
        int indexU = -1; //-1
        int cantidad = 0;
        indexU = v.getIndexUsuario();
        // indexU = validarRecepcionista(v);

        if (indexU != -1) {
            try {
                r = (Recepcionista) usuarios.get(indexU);
                codProducto = EntradaSalida.leerInt("Ingrese el codigo del producto: ");
            } catch (NumberFormatException ex) {
                EntradaSalida.mostrarString("Ingrese un codigo valido disponible");
            } catch (NullPointerException ex) {
                EntradaSalida.mostrarString("Ingrese un codigo valido disponible");
            }
            if (codProducto == 0) {
                EntradaSalida.mostrarString("ERROR: codigo no existe.");
            } else {
                indexP = v.buscarIndexProducto(codProducto);
                if (indexP == -1) {
                    EntradaSalida.mostrarString("Producto no encontrado");
                } else {
                    p = productos.get(indexP);
                    if (p.getCategoria().equals("MEDICAMENTO")) {
                        EntradaSalida.mostrarString("NO PODES VENDER MEDICAMENTOS");
                    } else {
                        confirmar = EntradaSalida.leerBoolean("Desea confirmar el venta del producto?");
                    }
                }
            }
            if (confirmar == true) {
                cantidad = EntradaSalida.leerInt("Ingrese la cantidad del producto: ");
                if (p.getStock() - cantidad >= 0) {
                    p.disminuirStock(cantidad);
                } else {
                    EntradaSalida.mostrarString("STOCK:" + p.getStock());

                    productos.set(indexP, p);
                    v.setProductos(productos);

                    r.sumarCantVentas(cantidad);
                    usuarios.set(indexU, r);
                    v.setUsuarios(usuarios);

                    int IDVenta = 1;
                    venta = v.buscarVenta(IDVenta);

                    if (venta == null) {
                        venta = new Venta(r.getUsername(), IDVenta, p.codProducto);
                        ventas.add(venta);
                        v.setVentas(ventas);
                    } else {
                        EntradaSalida.mostrarString("El producto no esta disponible por el momento");
                    }
                }
            }
        }
    }

    public String ingresarTipoAnimal() {
        char opc;
        TipoAnimal tAnimal = null;
        String tipoAnimal = null;
        opc = EntradaSalida.leerChar(
                "**********************************************\nTIPO DE ANIMAL \n**********************************************\n"
                + "[1]   PERRO\n"
                + "[2]   GATO\n"
                + "[3]   AVE\n"
                + "[4]   EXOTICO");
        switch (opc) {

            case '1':
                tipoAnimal = tAnimal.GATO.toString();

                break;
            case '2':
                tipoAnimal = tAnimal.PERRO.toString();

                break;
            case '3':
                tipoAnimal = tAnimal.AVE.toString();

                break;
            case '4':
                tipoAnimal = tAnimal.EXOTICO.toString();
                break;

            default:
                EntradaSalida.mostrarString("ERROR: Opcion invalida");
                opc = '*';
        }//Fin switch
        return tipoAnimal;
    }

    /* public int validarRecepcionista(Veterinaria v) {
        int indexR = 0;
        String username, password;
        username = EntradaSalida.leerString("Ingrese nombre de usuario: ");
        if (username.equals("")) {
            EntradaSalida.mostrarString("ERROR: usuario no valido.");
        } else {
            password = EntradaSalida.leerString("Ingrese password: ");
            if (password.equals("")) {
                EntradaSalida.mostrarString("ERROR: password no valida");
            } else {

                indexR = v.buscarIndexRecepcionista(username + ": " + password); //ACA ESTA EL ERROR
                if (indexR != -1) {
                    EntradaSalida.mostrarString("Si se encuentra registrado en el sistema.");
                
            }
        }
        return indexR;
    }
     */
    public GregorianCalendar ingresarFecha() {
        int anio, mes, dia, hora, minuto;
        GregorianCalendar fechaCita = new GregorianCalendar();

        anio = 2020;
        mes = ingresarMes();
        dia = ingresarDia();
        hora = EntradaSalida.leerInt("Ingrese una hora");
        minuto = EntradaSalida.leerInt("Ingrese los mimutos");

        fechaCita.set(anio, mes, dia, dia, minuto);

        return fechaCita;
    }

    public void sumarCantVentas(int cantidad) {
        this.cantidadDeVentasRealiz = this.cantidadDeVentasRealiz + cantidad;

    }

    public int ingresarMes() {
        int opc;
        int numMes = -1;
        opc = EntradaSalida.leerInt(
                "**********************************************\nINGRESAR MES \n**********************************************\n"
                + "[1]   ENERO\n"
                + "[2]   FEBRERO\n"
                + "[3]   MARZO\n"
                + "[4]   ABRIL\n"
                + "[5]   MAYO\n"
                + "[6]   JUNIO\n"
                + "[7]   JULIO\n"
                + "[8]   AGOSTO\n"
                + "[9]   SEPTIEMBRE\n"
                + "[10]   OCTUBRE\n"
                + "[11]   NOVIEMBRE\n"
                + "[12]   DICIEMBRE");
        switch (opc) {

            case 1:
                numMes = Mes.ENERO.ordinal();
                break;

            case 2:
                numMes = Mes.FEBRERO.ordinal();
                break;

            case 3:
                numMes = Mes.MARZO.ordinal();
                break;

            case 4:
                numMes = Mes.ABRIL.ordinal();
                break;

            case 5:
                numMes = Mes.MAYO.ordinal();
                break;

            case '6':
                numMes = Mes.JUNIO.ordinal();
                break;

            case 7:
                numMes = Mes.JULIO.ordinal();
                break;

            case 8:
                numMes = Mes.AGOSTO.ordinal();
                break;
            case '9':
                numMes = Mes.SEPTIEMBRE.ordinal();
                break;

            case 10:
                numMes = Mes.OCTUBRE.ordinal();
                break;
            case 11:
                numMes = Mes.NOVIEMBRE.ordinal();
                break;
            case 12:
                numMes = Mes.DICIEMBRE.ordinal();
                break;

            default:
                EntradaSalida.mostrarString("ERROR: Opcion invalida");
                opc = -1;
        }//Fin switch
        return numMes;
    }

    public int ingresarDia() {
        int opc;
        int numDia = -1;
        opc = EntradaSalida.leerInt(
                "**********************************************\nNUMERO DE DIA \n**********************************************\n"
                + "[1]   1\n"
                + "[2]   2\n"
                + "[3]   3\n"
                + "[4]   4\n"
                + "[5]   5\n"
                + "[6]   6\n"
                + "[7]   7\n"
                + "[8]   8\n"
                + "[9]   9\n"
                + "[10]  10\n"
                + "[11]  11\n"
                + "[12]  12\n"
                + "[13]  13\n"
                + "[14]  14\n"
                + "[15]  15\n"
                + "[16]  16\n"
                + "[17]  17\n"
                + "[18]  18\n"
                + "[19]  19\n"
                + "[20]  20\n"
                + "[21]  21\n"
                + "[22]  22\n"
                + "[23]  23\n"
                + "[24]  24\n"
                + "[25]  25\n"
                + "[26]  26\n"
                + "[27]  27\n"
                + "[28]  28\n"
                + "[29]  29\n"
                + "[30]  30\n"
                + "[31]  31");
        switch (opc) {

            case 1:
                numDia = 1;
                break;

            case 2:
                numDia = 2;
                break;

            case 3:
                numDia = 3;
                break;

            case 4:
                numDia = 4;
                break;

            case 5:
                numDia = 5;
                break;

            case '6':
                numDia = 6;
                break;

            case 7:
                numDia = 7;
                break;

            case 8:
                numDia = 8;
                break;
            case '9':
                numDia = 9;
                break;

            case 10:
                numDia = 10;
                break;
            case 11:
                numDia = 11;
                break;
            case 12:
                numDia = 12;
                break;
            case 13:
                numDia = 13;
                break;

            case 14:
                numDia = 14;
                break;

            case 15:
                numDia = 15;
                break;

            case 16:
                numDia = 16;
                break;

            case 17:
                numDia = 17;
                break;

            case 18:
                numDia = 18;
                break;

            case 19:
                numDia = 19;
                break;

            case 20:
                numDia = 20;
                break;
            case 21:
                numDia = 21;
                break;

            case 22:
                numDia = 22;
                break;
            case 23:
                numDia = 23;
                break;
            case 24:
                numDia = 24;
                break;
            case 25:
                numDia = 25;
                break;
            case 26:
                numDia = 26;
                break;
            case 27:
                numDia = 27;
                break;
            case 28:
                numDia = 28;
                break;
            case 29:
                numDia = 29;
                break;
            case 30:
                numDia = 30;
                break;

            default:
                EntradaSalida.mostrarString("ERROR: Opcion invalida");
                opc = -1;
        }//Fin switch
        return numDia;
    }

}
