package Veterinaria;

import java.io.Serializable;

/**
 *
 * @author Brigitte
 */
public class Venta implements Serializable {

    String username;
    int codVenta;
    int codProducto;
    MiFecha fecha;

    public Venta(String username, int codVenta, int codProducto) {
        this.username = username;
        this.codVenta = codVenta;
        this.fecha = new MiFecha();

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public MiFecha getFecha() {
        return fecha;
    }

    public int getCodVenta() {
        return codVenta;
    }

    public void setCodVenta(int codVenta) {
        this.codVenta = codVenta;
    }

    @Override
    public String toString() {
        return "             " + this.username + "                 " + codVenta + "               " + codProducto + "               " + this.fecha + "\n";
    }

}
