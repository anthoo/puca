package Veterinaria;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;

public class Veterinaria implements Serializable {

    private int indexUsuario;

    private ArrayList<Usuario> usuarios;

    private ArrayList<Producto> productos;

    private ArrayList<Turno> turnos;
    private ArrayList<Agenda> agendas;
    private ArrayList<Venta> ventas;

    public Veterinaria() {
        this.usuarios = new ArrayList<Usuario>();
        this.productos = new ArrayList<>();
        this.turnos = new ArrayList<>();
        this.ventas = new ArrayList<>();
        this.agendas = new ArrayList<>();

        this.indexUsuario = -1;
    }

    public int getIndexUsuario() {
        return indexUsuario;
    }

    public void setIndexUsuario(int indexUsuario) {
        this.indexUsuario = indexUsuario;
    }

    public ArrayList<Usuario> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(ArrayList<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    public void setProductos(ArrayList<Producto> productos) {
        this.productos = productos;
    }

    public void setVentas(ArrayList<Venta> ventas) {
        this.ventas = ventas;
    }

    public void agregarUsuario(Usuario u) {
        this.usuarios.add(u);
    }

    public ArrayList<Producto> getProductos() {
        return productos;
    }

    public void agregarProducto(Producto p) {
        this.productos.add(p);
    }

    public ArrayList<Turno> getTurnos() {
        return turnos;
    }

    public void agregarTurno(Turno t) {
        this.turnos.add(t);
    }

    public ArrayList<Venta> getVentas() {
        return ventas;
    }

    public void agregarVenta(Venta v) {
        this.ventas.add(v);
    }

    public ArrayList<Agenda> getAgendas() {
        return agendas;
    }

    public void agregarAgenda(Agenda a) {
        this.agendas.add(a);
    }

    public void mostrarUsuarios() {
        String listAdmin = "";
        String listRecep = "";
        String listVeter = "";
        if (usuarios.size() <= 1) {
            EntradaSalida.mostrarString("Aun no se registrado Usuarios.");
        } else {
            for (int i = 0; i < usuarios.size(); i++) {
                Usuario u = usuarios.get(i);
                //usuarios.get(i).mostrar();
                if (u instanceof Administrador) {
                    listAdmin = listAdmin + u.toString();
                }
                if (u instanceof Recepcionista) {
                    listRecep = listRecep + ((Recepcionista) u).aString();
                }
                if (u instanceof Veterinario) {
                    listVeter = listVeter + ((Veterinario) u).aString();
                }
            }
            EntradaSalida.mostrarString(
                    "                                                                  LISTADO DE USUARIOS                                           \n\n"
                    + "                                                                ADMINISTRADOR                                                                     \n"
                    + "**************************************************************************************************************************\n"
                    + "Username          Password            \n"
                    + "**************************************************************************************************************************\n"
                    + listAdmin
                    + "\n\n\n                                                             RECEPCIONISTA                                                                   \n"
                    + "**************************************************************************************************************************\n"
                    + "Username        Password            \n"
                    + "**************************************************************************************************************************\n"
                    + listRecep
                    + "\n\n                                                               VETERINARIO                                                                      \n"
                    + "**************************************************************************************************************************\n"
                    + "Username        Password                       ID               Especialidad                         Dias de atencion\n"
                    + "**************************************************************************************************************************\n"
                    + listVeter + "\n\n");
        }
    }

    public void mostrarProductos(boolean tipoUser) {
        String listProductosRegulares = "";
        String listTodosLosProductos = "";
        Producto p;
        if (productos.size() <= 0) {
            EntradaSalida.mostrarString("Aun no hay productos ingresados.");
        } else {
            for (int i = 0; i < productos.size(); i++) {
//                productos.get(i).mostrar();
                p = productos.get(i);
                if (!tipoUser && !p.getCategoria().equals("MEDICAMENTO")) {
                    listProductosRegulares = listProductosRegulares + productos.get(i).toString();
                } else {
                    listTodosLosProductos = listTodosLosProductos + productos.get(i).toString();
                }
            }
            if (tipoUser) {
                EntradaSalida.mostrarString("                                                      LISTADO DE TODOS LOS PRODUCTOS                                                  \n\n"
                        + "****************************************************************************************************************************\n"
                        + "CodProducto                 Descripcion                    Precio               Stock                              Categoria\n"
                        + "****************************************************************************************************************************\n"
                        + listTodosLosProductos);
            } else {
                EntradaSalida.mostrarString("                                                      LISTADO DE PRODUCTOS  REGULARES                                                \n\n"
                        + "****************************************************************************************************************************\n"
                        + "CodProducto                 Descripcion                    Precio               Stock                              Categoria\n"
                        + "****************************************************************************************************************************\n"
                        + listProductosRegulares);
            }
        }
    }

    public void mostrarVentas() {
        String listVentas = "";
        if (ventas.size() <= 0) {
            EntradaSalida.mostrarString("Aun NO hay VENTAS realizadas.");

        } else {
            for (int i = 0; i < ventas.size(); i++) {
                //ventas.get(i).mostrar();
                listVentas = listVentas + ventas.get(i).toString();
            }
            EntradaSalida.mostrarString("**************LISTADO DE VENTAS*************\n\nCodUsuario    CodVenta    CodProducto                 FechaDeVenta                                 \n" + listVentas);
        }
    }

    public void mostrarTurnos() {
        String listTurnos = "";
        if (turnos.size() <= 0) {
            EntradaSalida.mostrarString("Aun no hay turnos ingresados.");

        } else {
            for (int i = 0; i < turnos.size(); i++) {
//              libros.get(i).mostrar();
                listTurnos = listTurnos + turnos.get(i).toString();
            }
            EntradaSalida.mostrarString("                                                                   LISTADO DE TURNOS                                               \n\n"
                    + "*******************************************************************************************************************************************************\n"
                    + "Fecha                Hora                tipo de Animal               Mascota                   Duenio                           nroContacto           \n"
                    + "*******************************************************************************************************************************************************\n"
                    + listTurnos);            
        }
    }

    public Veterinaria deSerializar(String a) throws IOException, ClassNotFoundException {
        Veterinaria v;
        try (FileInputStream f = new FileInputStream(a); ObjectInputStream o = new ObjectInputStream(f)) {
            v = (Veterinaria) o.readObject();
        }
        return v;
    }

    public void serializar(String a) throws IOException {
        FileOutputStream f = new FileOutputStream(a);
        ObjectOutputStream o = new ObjectOutputStream(f);
        o.writeObject(this);
        o.close();
        f.close();
    }

    public Usuario buscarUsuario(String datos) {
        int i = 0;
        boolean encontrado = false;
        Usuario u = null;

        while (i < usuarios.size() && !encontrado) {
            u = usuarios.get(i);
            if (datos.equals(u.getUsername() + ": " + u.getPassword())) {
                encontrado = true;
                this.setIndexUsuario(i);
            } else {
                i++;
            }
        }
        if (!encontrado) {
            return null;
        } else {
            return u;
        }
    }

    public int buscarIndexVeterinario(int numVeterinario) {
        int i = 0;
        boolean encontrado = false;
        Usuario u = null;
        while (i < this.getUsuarios().size() && !encontrado) {
            u = usuarios.get(i);
            if (u instanceof Veterinario) {
                if (((Veterinario) u).getNroVeterinario() == numVeterinario) {
                    encontrado = true;
                } else {
                    i++;
                }
            } else {
                i++;
            }
        }

        if (!encontrado) {
            return -1;
        } else {
            return i;
        }

    }

    public int buscarIndexProducto(int codProducto) {
        int i = 0;
        boolean encontrado = false;
        Producto p = null;

        while (i < productos.size() && !encontrado) {
            p = productos.get(i);
            if (p.getCodProducto() == codProducto) {
                encontrado = true;
            } else {
                i++;
            }
        }
        if (!encontrado) {
            return -1;
        } else {
            return i;
        }
    }

    public int buscarIndexTurno(int idTurno) {
        int i = 0;
        boolean encontrado = false;
        Turno t = null;

        while (i < turnos.size() && !encontrado) {
            t = turnos.get(i);
            if (t.getCodTurno() == idTurno) {
                encontrado = true;
            } else {
                i++;
            }
        }
        if (!encontrado) {
            return -1;
        } else {
            return i;
        }
    }

    public int buscarIndexAgenda(int idAgenda) {
        int i = 0;
        boolean encontrado = false;
        Agenda a = null;

        while (i < agendas.size() && !encontrado) {
            a = agendas.get(i);
            if (a.getNroAgenda() == idAgenda) {
                encontrado = true;
            } else {
                i++;
            }
        }
        if (!encontrado) {
            return -1;
        } else {
            return i;
        }
    }

    public Agenda buscarAgenda(int idAgenda) {
        int i = 0;
        boolean encontrado = false;
        Agenda a = null;

        while (i < agendas.size() && !encontrado) {
            a = agendas.get(i);
            if (a.getNroAgenda() == idAgenda) {
                encontrado = true;
            } else {
                i++;
            }
        }
        if (!encontrado) {
            return null;
        } else {
            return a;
        }
    }

    public Producto buscarProducto(int codProducto) {
        int i = 0;
        boolean encontrado = false;
        Producto p = null;

        while (i < productos.size() && !encontrado) {
            p = productos.get(i);
            if (p.getCodProducto() == codProducto) {
                encontrado = true;
            } else {
                i++;
            }
        }
        if (!encontrado) {
            return null;
        } else {
            return p;
        }
    }

    public Venta buscarVenta(int IDVenta) {
        int i = 0;
        boolean encontrado = false;
        Venta venta = null;

        while (i < ventas.size() && !encontrado) {
            venta = ventas.get(i);
            if (venta.getCodVenta() == IDVenta) {
                encontrado = true;
            } else {
                i++;
            }
        }
        if (!encontrado) {
            return null;
        } else {
            return venta;
        }
    }

    /**/
    public void limpiar() {
        turnos.clear();
    }

    public void addTurno(Turno nuevo) {
        turnos.add(nuevo);
    }

    public void ordenarHora() {
        Collections.sort(turnos, new Comparator() {
            public int compare(Object o1, Object o2) {
                Turno t1 = (Turno) o1;
                Turno t2 = (Turno) o2;
                return t1.getGc().get(GregorianCalendar.HOUR) - t2.getGc().get(GregorianCalendar.HOUR);
            }

            public boolean equals(Object obj) {
                return true;
            }
        });
    }

    public Object getElementAt(int index) {
        return turnos.get(index);
    }

    public int getSize() {
        return turnos.size();
    }

}
