package Veterinaria;

import javax.swing.JOptionPane;

public class EntradaSalida {

     public static String leerString(String texto) {
        String st = JOptionPane.showInputDialog(texto);
        return (st == null ? "" : st);
    }

    public static boolean leerBoolean(String texto) {
        int i = JOptionPane.showConfirmDialog(null, texto, "Consulta", JOptionPane.YES_NO_OPTION);
        return i == JOptionPane.YES_OPTION;
    }

    public static void mostrarString(String s) {
        JOptionPane.showMessageDialog(null, s);
    }
    
     public static char leerChar(String texto) {
        String st = JOptionPane.showInputDialog(texto);
        return (st.length() > 0 ? st.charAt(0) : '0');
    }
     
     public static int leerInt(String texto) {
        String st = JOptionPane.showInputDialog(texto);
        return Integer.parseInt(st);
    }
}
