package Veterinaria;

import java.io.Serializable;

public class Veterinario extends Usuario implements Serializable {

    private static int IdSiguiente = 0;
    private int nroVeterinario;    

    private String especialidad;
    private String dias[] = {" ", " ", " "};

    public Veterinario(String username, String password, String especialidad, String dias[]) {
        super(username, password);
        IdSiguiente++;
        this.nroVeterinario = getIdSiguiente();
        this.especialidad = especialidad;
        this.dias = dias;

    }

    @Override
    public void setPassword(String password) {
        super.setPassword(password);
    }

    @Override
    public String getPassword() {
        return super.getPassword();
    }

    @Override
    public void setUsername(String username) {
        super.setUsername(username);
    }

    @Override
    public String getUsername() {
        return super.getUsername();
    }

    public static int getIdSiguiente() {
        return IdSiguiente;
    }

    public int getNroVeterinario() {
        return nroVeterinario;
    }

    public int getCantMascAt() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String toString() {
        return "                " + nroVeterinario + "                     " + especialidad + "                           " + this.verDias();
    }

    public String aString() {
        return super.toString() + toString() + "\n";
    }

    @Override
    public void mostrar() {
        EntradaSalida.mostrarString(super.toString() + this.toString());
    }

    public String[] getDias() {
        return dias;
    }

    public void setDias(String[] dias) {
        this.dias = dias;
    }

    @Override
    public boolean seguir(Veterinaria veterinaria) {
        char opc;
        boolean continuar = true;
        boolean tipoUser = true;
        EntradaSalida.mostrarString("          VETERINARIA\n           Bienvenido/a:           "
                + getUsername());
        do {
            opc = EntradaSalida.leerChar(
                    "**********************************************\nFUNCIONES DEL VETERINARIO\n**********************************************\n"
                    + "[1]   Atencion Medica de Animales"
                    + "os\n Vender Producto\n"
                    + "[2]   Cambiar de Usuario\n"
                    + "[3]   Salir del sistema");
            switch (opc) {

                case '1':
                    veterinaria.mostrarProductos(tipoUser);
                    veterinaria.mostrarTurnos();

                    break;
                case '2':
                    continuar = true;

                    break;
                case '3':
                    continuar = false;
                    break;
                default:
                    EntradaSalida.mostrarString("ERROR: Opcion invalida");
                    opc = '*';
            }//Fin switch

        } while (opc != '2' && opc != '3');//Fin while
        return continuar;
    }

    public Veterinaria venderProducto(Veterinaria v) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String verDias() {
        String dias = "";
        for (int i = 0; i < this.dias.length; i++) {
            dias = dias + this.dias[i] + "   ";
        }
        return dias;
    }
}
