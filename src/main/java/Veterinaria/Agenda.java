/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Veterinaria;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.GregorianCalendar;

/**
 *
 * @author Brigitte
 */
public class Agenda implements Serializable {
    //private ArrayList<Turno> turnos=new ArrayList<Turno>();
    private int nroAgenda;
    private int numVeterinario;   
    private int codTurno;

    public Agenda(int nroAgenda, int numVeterinario, int codTurno) {
        this.nroAgenda = nroAgenda;
        this.numVeterinario = numVeterinario;
        this.codTurno = codTurno;
    }    

    public int getNroAgenda() {
        return nroAgenda;
    }

    public int getNumVeterinario() {
        return numVeterinario;
    }

    public int getCodTurno() {
        return codTurno;
    }
    
    
   /** public void limpiar(){
        turnos.clear();
    }
    
    public void addTurno(Turno nuevo){
        turnos.add(nuevo);
    }
    
    public void ordenarHora(){
        Collections.sort(turnos, new Comparator() {
            public int compare(Object o1, Object o2) {
                Turno t1=(Turno)o1;
                Turno t2=(Turno)o2;
                return t1.getGc().get(GregorianCalendar.HOUR)-t2.getGc().get(GregorianCalendar.HOUR);
            }
            public boolean equals(Object obj) {
                return true;
            }
        });
    }

    public Object getElementAt(int index) {
        return turnos.get(index);
    }

    public int getSize() {
        return turnos.size();
    }

    */
}
