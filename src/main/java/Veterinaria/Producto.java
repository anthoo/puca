package Veterinaria;

import java.io.Serializable;

public class Producto implements Serializable {

    private static int ID = 0;
    int codProducto;
    private String descripcion;
    private float precio;
    int stock;
    private String categoria;

    public Producto(String descripcion, float precio, int stock, String categoria) {
        this.descripcion = descripcion;
        this.precio = precio;
        this.stock = stock;
        this.categoria = categoria;
        ID = getID() + 1;//Generador de codigo
        this.codProducto = ID;
    }

    public static int getID() {
        return ID;
    }

    
    public int getCodProducto() {
        return codProducto;
    }

    public void setCodProducto(int codProducto) {
        this.codProducto = codProducto;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
    public void disminuirStock(int cant) {
        this.stock = this.stock - cant;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String toString() {
        return " " + this.getCodProducto() + "                                    " + descripcion + "       "
                + "                  " + precio + "                   " + stock + "                                " + categoria + "\n\n";
    }

    public void mostrar() {
        EntradaSalida.mostrarString(this.toString());
    }
}
