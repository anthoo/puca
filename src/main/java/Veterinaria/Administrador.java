package Veterinaria;

import java.io.IOException;
import java.io.Serializable;

public class Administrador extends Usuario implements Serializable {

    public enum Especialidad {
        FELINA, CANINA, AVES, EXOTICOS
    }

    public enum Dias {
        LUNES, MARTES, MIERCOLES, JUEVES, VIERNES, SABADO
    }

    public enum Categoria {
        REGULAR, MEDICAMENTO
    }

    public Administrador(String username, String password) {
        super(username, password);
    }

    @Override
    public void setPassword(String password) {
        super.setPassword(password);
    }

    @Override
    public String getPassword() {
        return super.getPassword();
    }

    @Override
    public void setUsername(String username) {
        super.setUsername(username);
    }

    @Override
    public String getUsername() {
        return super.getUsername();
    }

    @Override
    public void mostrar() {
        EntradaSalida.mostrarString(super.toString() + "     administrador");
    }

    @Override
    public boolean seguir(Veterinaria veterinaria) {
        char op;
        boolean continuar = true;
        boolean tipoUser = true;

        do {
            op = EntradaSalida.leerChar(
                    "**********************************************\nFUNCIONES DEL ADMINISTRADOR\n**********************************************\n"
                    + "[1]   Dar de alta un recepcionista\n"
                    + "[2]   Dar de alta un Veterinario\n"
                    + "[3]   Mostrar Usuarios\n"
                    + "[4]   Agregar Producto\n"
                    + "[5]   Listado de Productos\n"
                    + "[6]   Cambiar de Usuario\n"
                    + "[7]   Salir del sistema");

            String r = "RECEPCIONISTA";
            String v = "VETERINARIO";
//            String l = "LIBRO";
            switch (op) {
                case '1':
                    cargarRecepcionistas(veterinaria, r);

                    break;
                case '2':
                    cargarVeterinarios(veterinaria, v);

                    break;
                case '3':
                    veterinaria.mostrarUsuarios();

                    break;
                case '4':
                    cargarProducto(veterinaria);
                    //  cargarTurno(veterinaria);

                    break;
                case '5':
                    veterinaria.mostrarProductos(tipoUser);

                    break;
                case '6':
                    continuar = true;

                    break;
                case '7':
                    continuar = false;

                    break;
                default:
                    EntradaSalida.mostrarString("ERROR: Opcion invalida");
                    op = '*';

            } //Fin switch
            if (op >= '1' && op <= '5') {
                try {
                    veterinaria.serializar("datos.txt");
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } while (op != '6' && op != '7'); //Fin while

        return continuar;

    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////

    public Administrador() {
        super(null, null);
    }

    public void cargarRecepcionistas(Veterinaria v, String tipoU) {
        registrarUsuarios(v, tipoU);
    }

    public void cargarVeterinarios(Veterinaria v, String tipoU) {
        registrarUsuarios(v, tipoU);
    }

    public Veterinaria cargarTurno(Veterinaria v) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    private void registrarUsuarios(Veterinaria v, String tipoU) {
        String username, password;
        username = EntradaSalida.leerString("ALTA DE " + tipoU + "\n" + "Ingrese nombre de usuario: ");
        if (username.equals("")) {
            EntradaSalida.mostrarString("ERROR: usuario no valido.");
        } else {
            password = EntradaSalida.leerString("Ingrese password: ");
            if (password.equals("")) {
                EntradaSalida.mostrarString("ERROR: password no valida");
            } else {
                Usuario u = v.buscarUsuario(username + ": " + password);

                if (u != null) {
                    EntradaSalida.mostrarString("ERROR: " + username + "Ya se encuentra registrado en el sistema.");
                } else {
                    if (tipoU.equals("RECEPCIONISTA")) {
                        u = new Recepcionista(username, password);
                        v.getUsuarios().add(u);
                        //b.agregarUsuario(u);
                    } else if (tipoU.equals("VETERINARIO")) {
                        u = new Veterinario(username, password, ingresarEspecialidad(), this.ingresarDias());
                        v.getUsuarios().add(u);
                        //b.agregarUsuario(u);
                    }
                    EntradaSalida.mostrarString("Se ha incorporado un nuevo " + tipoU + " al sistema.");
                }
            }
        }
    }

    public void cargarProducto(Veterinaria v) {
        int codProducto = 0;
        codProducto = EntradaSalida.leerInt("Ingrese codigo del Producto: ");
        if (codProducto == 0) {
            EntradaSalida.mostrarString("ERROR: codigo no existe.");
        } else {
            Producto p = v.buscarProducto(codProducto);
            if (p != null) {
                EntradaSalida.mostrarString("ERROR: " + codProducto + "Ya se encuentra registrado en el sistema.");
            } else {

                p = new Producto("producto X", (float) 200.00, 5, ingresarCategoria());
                v.getProductos().add(p);
                EntradaSalida.mostrarString("Se ha incorporado un nuevo producto: " + p.getDescripcion() + " al sistema.");
            }
        }
    }

    public String ingresarEspecialidad() {
        char opc;
        Especialidad e = null;
        String especialidad = null;
        opc = EntradaSalida.leerChar(
                "**********************************************\nESPECIALIDAD\n**********************************************\n"
                + "[1]   FELINA\n"
                + "[2]   CANINO\n"
                + "[3]   AVES\n"
                + "[4]   EXOTICOS");
        switch (opc) {

            case '1':
                especialidad = e.FELINA.toString();

                break;
            case '2':
                especialidad = e.CANINA.toString();

                break;
            case '3':
                especialidad = e.AVES.toString();

                break;
            case '4':
                especialidad = e.EXOTICOS.toString();
                break;

            default:
                EntradaSalida.mostrarString("ERROR: Opcion invalida");
                opc = '*';
        }//Fin switch
        return especialidad;
    }

    public String ingresarCategoria() {
        char opc;
        Categoria c = null;
        String categoria = null;
        opc = EntradaSalida.leerChar(
                "**********************************************\n                      CATEGORIA DEL PRODUCTO\n**********************************************\n"
                + "[1]   REGULAR\n"
                + "[2]   MEDICAMENTO\n"
        );
        switch (opc) {

            case '1':
                categoria = c.REGULAR.toString();

                break;
            case '2':
                categoria = c.MEDICAMENTO.toString();
                break;

            default:
                EntradaSalida.mostrarString("ERROR: Opcion invalida");
                opc = '*';
        }//Fin switch
        return categoria;
    }

    public String[] ingresarDias() {
        char opc;
        //boolean isMenor3 = false;
        //Dias d = null;
        String dias[] = new String[3];
        int i = 0;
        do {
            opc = EntradaSalida.leerChar(
                    "\n                                                        INGRESE TRES DIAS DE LA SEMANA                                                       \n"
                    + "*********************************************************************************************************************************************\n"
                    + "[1]   LUNES\n"
                    + "[2]   MARTES\n"
                    + "[3]   MIERCOLES\n"
                    + "[4]   JUEVES\n"
                    + "[5]   VIERNES\n"
                    + "[6]   SABADO");

            switch (opc) {
                case '1':
                    dias[i] = "LUNES";//d.LUNES.toString();
                    break;

                case '2':
                    dias[i] = Dias.MARTES.toString();

                    break;
                case '3':
                    dias[i] = Dias.MIERCOLES.toString();

                    break;
                case '4':
                    dias[i] = Dias.JUEVES.toString();
                    break;

                case '5':
                    dias[i] = Dias.VIERNES.toString();
                    break;

                case '6':
                    dias[i] = Dias.SABADO.toString();
                    break;

                default:
                    EntradaSalida.mostrarString("ERROR: Opcion invalida");
                    opc = '*';
            }//Fin switch
            i++;
        } while (i < 3);
        return dias;
    }
}
