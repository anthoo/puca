package Veterinaria;

import java.io.Serializable;
import java.util.Calendar;
import java.util.GregorianCalendar;

public class Turno implements Serializable {

    private static int IdSiguiente = 0;
    private int idX;

    private int codTurno;
    private GregorianCalendar gc;
    private String tipoAnimmal;
    boolean isConfirmado;

    private String nomMascota;
    private String nombDuenio;
    private String nroContacto;

    public Turno(int codTurno, GregorianCalendar gc, String tipoAnimmal, boolean isConfirmado) {
        this.codTurno = codTurno;
        this.gc = gc;
        this.tipoAnimmal = tipoAnimmal;
        this.isConfirmado = isConfirmado;

        IdSiguiente++;
        this.idX = getIdSiguiente();
        this.nomMascota = " Mascota " + this.getIdX();
        this.nombDuenio = " Dueño  " + this.getIdX();
        this.nroContacto = " Contacto " + this.getIdX() + " @gmail.com ";

    }

    public static int getIdSiguiente() {
        return IdSiguiente;
    }

    public int getIdX() {
        return idX;
    }

    public GregorianCalendar getGc() {
        return gc;
    }

    public void setGc(GregorianCalendar gc) {
        this.gc = gc;
    }

    public String getTipoAnimmal() {
        return tipoAnimmal;
    }

    public void setTipoAnimmal(String tipoAnimmal) {
        this.tipoAnimmal = tipoAnimmal;
    }

    public boolean isIsConfirmado() {
        return isConfirmado;
    }

    public void setIsConfirmado(boolean isConfirmado) {
        this.isConfirmado = isConfirmado;
    }

    public int getCodTurno() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getNomMascota() {
        return nomMascota;
    }

    public void setNomMascota(String nomMascota) {
        this.nomMascota = nomMascota;
    }

    public String getNombDuenio() {
        return nombDuenio;
    }

    public void setNombDuenio(String nombDuenio) {
        this.nombDuenio = nombDuenio;
    }

    public String getNroContacto() {
        return nroContacto;
    }

    public void setNroContacto(String nroContacto) {
        this.nroContacto = nroContacto;
    }

    
    @Override
    public String toString() {       
        //int hora = gc.get(GregorianCalendar.HOUR_OF_DAY);
        // return hora + "- " + ", tipoAnimmal=" + tipoAnimmal + ", nomMascota=" + nomMascota + ", nombDuenio=" + nombDuenio + ", nroContacto=" + nroContacto + "\n";
        int anio = this.getGc().get(GregorianCalendar.YEAR);
        int mes = this.getGc().get(GregorianCalendar.MONTH);
        int dia = this.getGc().get(GregorianCalendar.DAY_OF_MONTH);
        int hora = this.getGc().get(GregorianCalendar.HOUR_OF_DAY);
        int minuto = this.getGc().get(GregorianCalendar.MINUTE);
        
        return dia + "/" + (mes+1) + "/" + anio + "          " + hora + ": " +  minuto + "                       " + tipoAnimmal + "                     " + nomMascota + "                " + nombDuenio + "                  " + nroContacto + "\n";
    }
    

    public void mostrar() {
    }
}
