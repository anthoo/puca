
package Veterinaria;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MiFecha extends Date{
public MiFecha (){
 }
 public MiFecha (long ms){
 super(ms);
 }
@Override
 public String toString(){
 String s = new SimpleDateFormat("EEEE',' dd 'de' MMMM 'de' yyyy" ).format(this);
 return s.substring(0, 1).toUpperCase() + s.substring(1);
 }    
}
